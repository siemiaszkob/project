package projmgmt.validator;

/**
 * @author lulewiczg
 *
 */
public interface LoginValidator {
	
	/**
	 * Interface method
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean validateUser(String username, String password);
	
}
