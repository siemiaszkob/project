package projmgmt.validator;


public class DummyLoginValidator implements LoginValidator {


	/* (non-Javadoc)
	 * @see projmgmt.validator.LoginValidator#validateUser(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean validateUser(String username, String password) {
		
		if ("wcadmin".equals(username) && "wcadmin".equals(password)) {
			return true;
		} 
		return false;
	}
	

	
	
}
