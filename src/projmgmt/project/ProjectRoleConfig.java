package projmgmt.project;

import java.util.List;

import projmgmt.model.Role;
import projmgmt.model.User;

public interface ProjectRoleConfig {

	/**
	 * @param role
	 * @param user
	 */
	void addUserToRole(Role role, User user);
	
	/**
	 * @param user
	 * @return
	 */
	List<Role> getRoleForUser(User user);
	
	/**
	 * @param role
	 * @return
	 */
	List<User> getUsersForRole(Role role);
	
}