package projmgmt.project;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import projmgmt.model.Role;
import projmgmt.model.User;


public class RoleConfigMapImplementation implements ProjectRoleConfig {

	Map<Role,List<User>> roleToUserListMap = new HashMap<Role, List<User>>();

	
	@Override
	public void addUserToRole(Role role, User user) {
		if (roleToUserListMap.containsKey(role)) {
			List<User> userList = roleToUserListMap.get(role);
			userList.add(user);
		} else {
			List<User> userList = new LinkedList<User>();
			userList.add(user);
			
			roleToUserListMap.put(role, userList);
		}
		
	}

	@Override
	public List<Role> getRoleForUser(User user) {
		
		List<Role> roleList = new LinkedList<Role>();
		
		for (Entry<Role, List<User>> entry : roleToUserListMap.entrySet()) {
			if (entry.getValue() != null || entry.getValue().contains(user)) {
				roleList.add(entry.getKey());
			}
		}
		
		return roleList;
	}

	@Override
	public List<User> getUsersForRole(Role role) {
		
		return roleToUserListMap.get(role);
	}
	
	
	
	
	
}
