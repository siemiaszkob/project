package projmgmt.exception;

/**
 * @author lulewiczg
 *
 */
public class ProjMgmtException extends Exception {

	String message;

	public String getMessage() {
		return message;
	}

	/**
	 * 
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @param message
	 */
	public ProjMgmtException(String message) {
		super();
		this.message = message;
	}
	
}
