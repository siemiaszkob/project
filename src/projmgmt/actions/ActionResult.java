package projmgmt.actions;

public class ActionResult {
	
	// internal constants
	private static final String OK_STATUS_STRING = "OK";
	private static final String FAIL_STATUS_STRING = "FAIL";

	public static final ActionResult OK = new ActionResult(OK_STATUS_STRING); 
	public static final ActionResult FAIL = new ActionResult(FAIL_STATUS_STRING); 	

	private String actionStatus;
	private String errorMessage;
	
	// a generic return object that may be needed...
	private Object returnObject;
	
	/**
	 * @param actionStatus
	 */
	public ActionResult(String actionStatus) {
		this.actionStatus = actionStatus;
	}
	
	/**
	 * @return
	 */
	public String getActionStatus() {
		return actionStatus;
	}

	/**
	 * @param actionStatus
	 */
	public void setActionStatus(String actionStatus) {
		this.actionStatus = actionStatus;
	}

	/**
	 * @return
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return
	 */
	public Object getReturnObject() {
		return returnObject;
	}

	/**
	 * @param returnObject
	 */
	public void setReturnObject(Object returnObject) {
		this.returnObject = returnObject;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((actionStatus == null) ? 0 : actionStatus.hashCode());
		result = prime * result
				+ ((errorMessage == null) ? 0 : errorMessage.hashCode());
		result = prime * result
				+ ((returnObject == null) ? 0 : returnObject.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActionResult other = (ActionResult) obj;
		if (actionStatus == null) {
			if (other.actionStatus != null)
				return false;
		} else if (!actionStatus.equals(other.actionStatus))
			return false;
		if (errorMessage == null) {
			if (other.errorMessage != null)
				return false;
		} else if (!errorMessage.equals(other.errorMessage))
			return false;
		if (returnObject == null) {
			if (other.returnObject != null)
				return false;
		} else if (!returnObject.equals(other.returnObject))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ActionResult [actionStatus=" + actionStatus + ", errorMessage="
				+ errorMessage + ", returnObject=" + returnObject + "]";
	}
	
	
	
}
