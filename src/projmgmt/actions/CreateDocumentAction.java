package projmgmt.actions;

import java.util.Arrays;
import java.util.List;

import projmgmt.context.ApplicationContext;
import projmgmt.model.Role;
import projmgmt.persistence.PersistenceManager;
import projmgmt.userinterface.UserInterface;

public class CreateDocumentAction extends Action {

	/**
	 * @param userInterface
	 * @param persistenceManager
	 */
	public CreateDocumentAction(UserInterface userInterface,
			PersistenceManager persistenceManager) {
		super(userInterface, persistenceManager);

	}

	/* (non-Javadoc)
	 * @see projmgmt.actions.Action#doOperation()
	 */
	@Override
	public ActionResult doOperation() {
		try {
			throw new Exception ("Creating a document! Not implemented yet!");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see projmgmt.actions.Action#getRolesForWhichActionEnabled()
	 */
	@Override
	public List<Role> getRolesForWhichActionEnabled() {
		return Arrays.asList(Role.MANAGER);
	}

	/* (non-Javadoc)
	 * @see projmgmt.actions.Action#getDisplayName()
	 */
	@Override
	public String getDisplayName() {
		return "Create Document";
	}

}
