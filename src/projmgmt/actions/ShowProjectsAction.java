package projmgmt.actions;

import java.util.Arrays;
import java.util.List;

import projmgmt.context.ApplicationContext;
import projmgmt.exception.ProjMgmtException;
import projmgmt.model.Role;
import projmgmt.persistence.PersistenceManager;
import projmgmt.userinterface.UserInterface;

public class ShowProjectsAction extends Action {

	/**
	 * @param userInterface
	 * @param persistenceManager
	 */
	public ShowProjectsAction(UserInterface userInterface,
			PersistenceManager persistenceManager) {
		super(userInterface, persistenceManager);
	}

	/* (non-Javadoc)
	 * @see projmgmt.actions.Action#doOperation()
	 */
	@Override
	public ActionResult doOperation() throws ProjMgmtException {

		// if (true) is here t make the code compile...
		if (true) {
			throw new ProjMgmtException("Creating a document! Not implemented yet!");			
		}
		
		return ActionResult.FAIL;
	}

	/* (non-Javadoc)
	 * @see projmgmt.actions.Action#getRolesForWhichActionEnabled()
	 */
	@Override
	public List<Role> getRolesForWhichActionEnabled() {
		return Arrays.asList(Role.ENGINEER, Role.MANAGER);
	}

	/* (non-Javadoc)
	 * @see projmgmt.actions.Action#getDisplayName()
	 */
	@Override
	public String getDisplayName() {
		return "Show Projects";
	}

}
