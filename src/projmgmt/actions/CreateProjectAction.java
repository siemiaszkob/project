package projmgmt.actions;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import projmgmt.model.Project;
import projmgmt.model.Role;
import projmgmt.persistence.PersistenceManager;
import projmgmt.userinterface.UserInterface;

public class CreateProjectAction extends Action {

	public CreateProjectAction(UserInterface userInterface,
			PersistenceManager persistenceManager) {
		super(userInterface, persistenceManager);

	}

	private static final String CREATE_PROJECT = "Create Project";
	private static final String PROJECT_NAME_PARAMETER = "Project Name";
	private static final String PROJECT_DESCRIPTION_PARAMETER = "Project Description";
	

	/* (non-Javadoc)
	 * @see projmgmt.actions.Action#doOperation()
	 */
	@Override
	public ActionResult doOperation() {
		
		Map<String, String> parameters = userInterface.getParameters(
				Arrays.asList(PROJECT_NAME_PARAMETER, PROJECT_DESCRIPTION_PARAMETER));
		
		String projectName = parameters.get(PROJECT_NAME_PARAMETER);
		String projectDescription = parameters.get(PROJECT_DESCRIPTION_PARAMETER);
		
		Project project = new Project(projectName, projectDescription);
		
		persistenceManager.save(project);
		
		userInterface.showMessage("Project created!");
			
		return ActionResult.OK;
				
	}

	/* (non-Javadoc)
	 * @see projmgmt.actions.Action#getRolesForWhichActionEnabled()
	 */
	@Override
	public List<Role> getRolesForWhichActionEnabled() {
		return new LinkedList<Role>();
	}

	/* (non-Javadoc)
	 * @see projmgmt.actions.Action#getDisplayName()
	 */
	@Override
	public String getDisplayName() {
		return CREATE_PROJECT;
	}
	
	

}
