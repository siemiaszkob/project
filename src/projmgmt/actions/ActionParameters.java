package projmgmt.actions;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lulewiczg
 *
 */
public class ActionParameters {
	
	private Map<String,Object> paramMap = new HashMap<String,Object>();
	
	/**
	 * @param paramName
	 * @return
	 */
	public Object getParam(String paramName) {
		return paramMap.get(paramName);
	}
	
	public void setParam(String paramName, Object paramValue) {
		paramMap.put(paramName, paramValue);
	}

}
