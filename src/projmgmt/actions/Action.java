package projmgmt.actions;

import java.util.List;

import projmgmt.context.ApplicationContext;
import projmgmt.exception.ProjMgmtException;
import projmgmt.model.Role;
import projmgmt.persistence.PersistenceManager;
import projmgmt.userinterface.UserInterface;

public abstract class Action {
	
	protected UserInterface userInterface;
	protected PersistenceManager persistenceManager;
		
	/**
	 * @param userInterface
	 * @param persistenceManager
	 */
	public Action(UserInterface userInterface,
			PersistenceManager persistenceManager) {
		super();
		this.userInterface = userInterface;
		this.persistenceManager = persistenceManager;
	}

	/**
	 * @return
	 */
	abstract public String getDisplayName();
	
	/**
	 * @return
	 * @throws ProjMgmtException
	 */
	abstract public ActionResult doOperation() throws ProjMgmtException;

	/**
	 * @return
	 */
	abstract public List<Role> getRolesForWhichActionEnabled();
	
	
}
