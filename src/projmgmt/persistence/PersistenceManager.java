package projmgmt.persistence;

import java.util.Collection;

import projmgmt.model.Project;
import projmgmt.model.User;

public interface PersistenceManager {
	
	/**
	 * @param project
	 * @return
	 */
	public Project save(Project project);

	/**
	 * @param user
	 * @return
	 */
	public User save(User user);
	
	// and other methods...
	
	/**
	 * @param objectClass
	 * @param filterCriteria
	 * @return
	 */
	public Collection<Object> find(Class objectClass, FilterCriteria filterCriteria);
	
}
