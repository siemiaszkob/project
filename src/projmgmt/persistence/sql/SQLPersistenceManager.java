package projmgmt.persistence.sql;

import java.util.Collection;

import org.apache.log4j.Logger;

import projmgmt.model.Project;
import projmgmt.model.User;
import projmgmt.persistence.FilterCriteria;
import projmgmt.persistence.PersistenceManager;
import projmgmt.userinterface.console.ConsoleUserInterface;

/**
 * @author lulewiczg
 *
 */
public class SQLPersistenceManager implements PersistenceManager {

	private static SQLPersistenceManager persistenceManager;
	private Logger logger = Logger.getLogger(SQLPersistenceManager.class);
	// hide the default constuctor
	/**
	 * 
	 */
	private SQLPersistenceManager () {
	}
	
	// a singleton database connection
	/**
	 * @return
	 */
	public static PersistenceManager getInstance() {
		if (persistenceManager == null) {
			return new SQLPersistenceManager();
		} else {
			return persistenceManager;
		}
	}

	/* (non-Javadoc)
	 * @see projmgmt.persistence.PersistenceManager#save(projmgmt.model.Project)
	 */
	@Override
	public Project save(Project project) {
		logger.error("Project saving not implemented!");
		return null;
	}

	/* (non-Javadoc)
	 * @see projmgmt.persistence.PersistenceManager#save(projmgmt.model.User)
	 */
	@Override
	public User save(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see projmgmt.persistence.PersistenceManager#find(java.lang.Class, projmgmt.persistence.FilterCriteria)
	 */
	@Override
	public Collection<Object> find(Class objectClass,
			FilterCriteria filterCriteria) {
		// TODO Auto-generated method stub
		return null;
	}

}
