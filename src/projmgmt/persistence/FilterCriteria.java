package projmgmt.persistence;

import java.util.HashMap;
import java.util.Map;

public class FilterCriteria {
	
	private Map<String, String> fieldNameValueMap = new HashMap<String, String>();

	/**
	 * @return
	 */
	public Map<String, String> getFieldNameValueMap() {
		return fieldNameValueMap;
	}

	/**
	 * @param fieldNameValueMap
	 */
	public void setFieldNameValueMap(Map<String, String> fieldNameValueMap) {
		this.fieldNameValueMap = fieldNameValueMap;
	}

	/**
	 * @param name
	 * @param value
	 */
	public void addFilterCriterium(String name, String value) {
		fieldNameValueMap.put(name, value);
	}
	
	/**
	 * @param fieldName
	 * @return
	 */
	public String get(String fieldName) {
		return fieldNameValueMap.get(fieldName);
	}
	
	
}
