package projmgmt.context;

import java.util.Collection;

import projmgmt.actions.Action;
import projmgmt.model.Project;
import projmgmt.model.User;
import projmgmt.userinterface.UserInterface;



public class ApplicationContext {
	
	static int userCount;
	
	Collection<Action> actionList;
	
	User user;
	
	UserInterface userInterface;
	
	private boolean shouldExitApplication = true;
	
	Project project = null;
	
	/**
	 * @return
	 */
	public Collection<Action> getActionList() {
		return actionList;
	}

	/**
	 * @param actionList
	 */
	public void setActionList(Collection<Action> actionList) {
		this.actionList = actionList;
	}

	/**
	 * @return
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 */
	public void setUser(User user) {
		
		userCount ++;
		
		this.user = user;
	}

	/**
	 * @return
	 */
	public UserInterface getUserInterface() {
		return userInterface;
	}

	/**
	 * @param userInterface
	 */
	public void setUserInterface(UserInterface userInterface) {
		this.userInterface = userInterface;
	}
	
	/**
	 * @return
	 */
	public boolean shouldExitApplication() {
		return shouldExitApplication;
	}

	/**
	 * @return
	 */
	public Project getProject() {
		
		
		
		return project;
	}


}
