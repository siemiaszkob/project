package projmgmt.startup.config;

import java.util.Collection;
import java.util.LinkedList;

import projmgmt.actions.Action;
import projmgmt.actions.ActionValidator;
import projmgmt.actions.CreateDocumentAction;
import projmgmt.actions.CreateProjectAction;
import projmgmt.actions.ShowProjectsAction;
import projmgmt.persistence.PersistenceManager;
import projmgmt.persistence.sql.SQLPersistenceManager;
import projmgmt.userinterface.UserInterface;
import projmgmt.userinterface.console.ConsoleUserInterface;
import projmgmt.validator.LoginValidator;

/**
 * @author lulewiczg
 *
 */
public class MainConfig {

	private UserInterface userInterface;
	private PersistenceManager persistenceManager;
	private Collection<Action> actions;
	private LoginValidator loginValidator;
	private ActionValidator actionValidator;
	
	
	/**
	 * @return
	 */
	public ActionValidator getActionValidator() {
		return actionValidator;
	}

	/**
	 * @param actionValidator
	 */
	public void setActionValidator(ActionValidator actionValidator) {
		this.actionValidator = actionValidator;
	}

	/**
	 * @return
	 */
	public LoginValidator getLoginValidator() {
		return loginValidator;
	}

	/**
	 * @param loginValidator
	 */
	public void setLoginValidator(LoginValidator loginValidator) {
		this.loginValidator = loginValidator;
	}

	/**
	 * @return
	 */
	public UserInterface getUserInterface() {
		return userInterface;
	}

	/**
	 * @param userInterface
	 */
	public void setUserInterface(UserInterface userInterface) {
		this.userInterface = userInterface;
	}

	/**
	 * @return
	 */
	public PersistenceManager getPersistenceManager() {
		return persistenceManager;
	}

	/**
	 * @param persistenceManager
	 */
	public void setPersistenceManager(PersistenceManager persistenceManager) {
		this.persistenceManager = persistenceManager;
	}

	/**
	 * @return
	 */
	public Collection<Action> getActions() {
		return actions;
	}

	/**
	 * @param actions
	 */
	public void setActions(Collection<Action> actions) {
		this.actions = actions;
	}

	// hide the default constructor
	private MainConfig () {
		
	}
	
	/**
	 * @return
	 */
	public static MainConfig createDevelopmentConfig() {

		MainConfig developmentConfig = new MainConfig();
		ConsoleUserInterface userInterface = ConsoleUserInterface.getUserInterface();
		PersistenceManager persistenceManager = SQLPersistenceManager.getInstance();
		
		// an anonymous implementation for testing...
		LoginValidator loginValidator = new LoginValidator() {
			@Override
			public boolean validateUser(String username, String password) {
				if ("wcadmin".equals(username) && "wcadmin".equals(password)) {
					return true;
				} 
				return false;
			}
		};
		
		ActionValidator actionValidator = new ActionValidator();
		
		
		developmentConfig.setUserInterface(userInterface);
		developmentConfig.setPersistenceManager(persistenceManager);
		developmentConfig.setLoginValidator(loginValidator);
		developmentConfig.setActions(initializeActions(userInterface,persistenceManager));
		developmentConfig.setActionValidator(actionValidator);
		
		return developmentConfig;
	}
	
	/**
	 * @param userInterface
	 * @param persistenceManager
	 * @return
	 */
	private static Collection<Action> initializeActions(ConsoleUserInterface userInterface, PersistenceManager persistenceManager) {

		Collection<Action> actions = new LinkedList<Action>();
		actions.add(new ShowProjectsAction(userInterface, persistenceManager));
		actions.add(new CreateProjectAction(userInterface, persistenceManager));
		actions.add(new CreateDocumentAction(userInterface, persistenceManager));

		return actions;

	}


}
