package projmgmt.startup.config;

/**
 * @author lulewiczg
 *
 */
public class SessionHelper {
	
	private static String loggedUserName;

	// mmethod reachable only from this package
	/**
	 * @param loggedUsername
	 */
	static void setLoggedUser(String loggedUsername) {
		SessionHelper.loggedUserName = loggedUsername;
	}
	
	/**
	 * @return
	 */
	public static String getLoggedUserName() {
		return loggedUserName;
	}


}
