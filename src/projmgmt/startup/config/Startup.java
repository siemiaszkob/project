package projmgmt.startup.config;


import org.apache.log4j.BasicConfigurator;

import projmgmt.context.ApplicationContext;

/**
 * @author lulewiczg
 *
 */
public class Startup {
	
	private static ApplicationContext applicationContext;
	
	public static void main(String[] args) {
		BasicConfigurator.configure();
		MainConfig mainConfig = MainConfig.createDevelopmentConfig();
		
		ProjectManagement projectManagement = new ProjectManagement(mainConfig);
		
		projectManagement.runApplication();
		
	}
	
	
	
	
	
	

}
