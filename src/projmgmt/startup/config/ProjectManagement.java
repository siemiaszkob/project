package projmgmt.startup.config;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import projmgmt.actions.Action;
import projmgmt.actions.ActionValidator;
import projmgmt.exception.ProjMgmtException;
import projmgmt.persistence.PersistenceManager;
import projmgmt.userinterface.UserInterface;
import projmgmt.validator.LoginValidator;

public class ProjectManagement {
	
	private UserInterface userInterface;
	private PersistenceManager persistenceManager;
	private LoginValidator loginValidator;
	private ActionValidator actionValidator;
	private Collection<Action> actions;

	private static final String USERNAME_PARAMETER = "username";
	private static final String PASSWORD_PARAMETER = "password";

	/**
	 * @param appConfig
	 */
	public ProjectManagement(MainConfig appConfig) {
		userInterface = appConfig.getUserInterface();
		persistenceManager = appConfig.getPersistenceManager();
		loginValidator = appConfig.getLoginValidator();
		actions = appConfig.getActions();
		actionValidator = appConfig.getActionValidator();
		
	}
	

	
	/**
	 * 
	 */
	public void runApplication() {
		
		loginUser();
		
		while (true) {
			
			showAndPerformAction();
		}
		
		
	}

	/**
	 * 
	 */
	private void showAndPerformAction() {
		Collection<Action> validatedActions = actionValidator.validateActions(actions);
		
		try {
			userInterface.displayAndExecuteActions(validatedActions);
		
		} catch (ProjMgmtException e) {
			userInterface.showException(e);
		}
		
	}

	/**
	 * 
	 */
	private void loginUser() {

		userInterface.displayWelcome();
		boolean loginValidated = false;
		
		while (!loginValidated) {
			
			Map<String, String> parameters = userInterface.getParameters(Arrays.asList(USERNAME_PARAMETER, PASSWORD_PARAMETER));
			String username = parameters.get(USERNAME_PARAMETER);
			String password = parameters.get(PASSWORD_PARAMETER);
			
			loginValidated = loginValidator.validateUser(username, password);
			
			if (loginValidated) {
				SessionHelper.setLoggedUser(username);
			} else {
				userInterface.showMessage("Wrong username and password, please try again");				
			}
		}
	}

	

}
