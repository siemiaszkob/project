package projmgmt.userinterface.console;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import projmgmt.actions.Action;
import projmgmt.exception.ProjMgmtException;
import projmgmt.userinterface.UserInterface;


public class ConsoleUserInterface implements UserInterface {

	private static final int WRONG_OR_NO_INPUT_USER_CHOICE = -1;
	private static final String WELCOME_MESSAGE = "Welcome to project management\n";
	private static final String LOGIN_MESSAGE = "please input your name: \n";
	private static final String LOGIN_PASSWORD = "please input your password: \n";
	private static final String PROJECT_NAME = "please input project name: \n";
	private static final String PROJECT_DESCRIPTION = "please input project description: \n";
	private static final String WRONG_LOGIN = "login wrong! please log in again: \n =========================== \n\n";
	
	private static ConsoleUserInterface consoleUserInterface = new ConsoleUserInterface();
	private BufferedReader userInputBufferedReader;
	private Logger logger = Logger.getLogger(ConsoleUserInterface.class);
	/**
	 * @return
	 */
	public static ConsoleUserInterface getUserInterface() {
		
		if (consoleUserInterface == null) {
			return new ConsoleUserInterface();
		} else {
			return consoleUserInterface;
		}
	}
	
	/**
	 * 
	 */
	private ConsoleUserInterface() {
		
		userInputBufferedReader = new BufferedReader(new InputStreamReader(System.in));
	}
	
	
	/**
	 * @return
	 */
	private String getLine() {
		String username = "";
		
		try {
			username = userInputBufferedReader.readLine();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return username;
	}

	


	/* (non-Javadoc)
	 * @see projmgmt.userinterface.UserInterface#displayWelcome()
	 */
	@Override
	public void displayWelcome() {	
		logger.info(WELCOME_MESSAGE);
		
	}


	/* (non-Javadoc)
	 * @see projmgmt.userinterface.UserInterface#displayAndExecuteActions(java.util.Collection)
	 */
	@Override
	public void displayAndExecuteActions(Collection<Action> actionsCollection) throws ProjMgmtException {
		
		Map<Integer, Action> actionsMap = new HashMap<Integer, Action>();
		
		int actionIndex = 1;
		logger.info("Please choose an action:");
		
		for (Action action : actionsCollection) {
			
			logger.info(actionIndex + " : " + action.getDisplayName() + "\n");
			
			actionsMap.put(actionIndex,action);
			actionIndex = actionIndex + 1;
			
		}
		
		
		logger.info("===========================\n\n");
		
		
		Action chosenAction = getChosenAction(actionsMap);
		
		chosenAction.doOperation();

		
	}

	/**
	 * @param actionsMap
	 * @return
	 */
	private Action getChosenAction(Map<Integer, Action> actionsMap) {
		Action chosenAction = null;
		
		Integer userChoiceInteger = WRONG_OR_NO_INPUT_USER_CHOICE;
		
		while (userChoiceInteger.equals(WRONG_OR_NO_INPUT_USER_CHOICE)) {
			String userChoiceString = getLine();
			
			try {
				userChoiceInteger = Integer.parseInt(userChoiceString);
				
				if (!actionsMap.keySet().contains(userChoiceInteger)) {
					userChoiceInteger = wrongUserChoice();
				} else {
					chosenAction = actionsMap.get(userChoiceInteger);
				}

				
			} catch (NumberFormatException numberFormatException) {
				userChoiceInteger = wrongUserChoice();
			}			
		}
		
		return chosenAction;
	}

	/**
	 * @return
	 */
	private Integer wrongUserChoice() {
		Integer userChoiceInteger;
		logger.info("wroong Input");
		userChoiceInteger = WRONG_OR_NO_INPUT_USER_CHOICE;
		return userChoiceInteger;
	}

	/* (non-Javadoc)
	 * @see projmgmt.userinterface.UserInterface#getParameters(java.util.List)
	 */
	@Override
	public Map<String, String> getParameters(List<String> parameters) {
		
		Map<String,String> parameterMap = new HashMap<String, String>();
		
		logger.info("please provide the following parameters: ");
		
		for (String parameter : parameters) {
			logger.info(parameter + ": ");
			String value = getLine();
			
			parameterMap.put(parameter, value);
		}
		
		return parameterMap;
		
	}

	/* (non-Javadoc)
	 * @see projmgmt.userinterface.UserInterface#showMessage(java.lang.String)
	 */
	@Override
	public void showMessage(String message) {
		logger.info(message);
		
	}

	/* (non-Javadoc)
	 * @see projmgmt.userinterface.UserInterface#showException(projmgmt.exception.ProjMgmtException)
	 */
	@Override
	public void showException(ProjMgmtException e) {
		logger.error(e.getMessage());
		
	}

	


}













