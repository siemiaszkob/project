package projmgmt.userinterface;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import projmgmt.actions.Action;
import projmgmt.context.ApplicationContext;
import projmgmt.exception.ProjMgmtException;

public interface UserInterface {
	
	Map<String, String> getParameters(List<String> parameters);
	
	/**
	 * @param message
	 */
	void showMessage(String message);
	
	/**
	 * 
	 */
	void displayWelcome();

	
	/**
	 * @param actionsCollection
	 * @throws ProjMgmtException
	 */
	void displayAndExecuteActions(Collection<Action> actionsCollection) throws ProjMgmtException;
	

	/**
	 * @param e
	 */
	void showException(ProjMgmtException e);

}
