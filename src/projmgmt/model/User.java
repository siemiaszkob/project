package projmgmt.model;

public class User {

	String name;
	boolean isGlobalAdmin;
	
	/**
	 * @param name
	 * @param isGlobalAdmin
	 */
	public User(String name, boolean isGlobalAdmin) {
		this.name = name;
		this.isGlobalAdmin = isGlobalAdmin;
	}
	
	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return
	 */
	public boolean isGlobalAdmin() {
		return isGlobalAdmin;
	}

	/**
	 * @param isGlobalAdmin
	 */
	public void setGlobalAdmin(boolean isGlobalAdmin) {
		this.isGlobalAdmin = isGlobalAdmin;
	}	
	
	
}
