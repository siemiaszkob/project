package projmgmt.model;

import projmgmt.project.ProjectRoleConfig;
import projmgmt.project.RoleConfigMapImplementation;

/**
 * @author lulewiczg
 *
 */
public class Project {
	
	String name;
	
	String description;
		
	ProjectRoleConfig roleConfig;
	
	
	
	/**
	 * @param name
	 * @param description
	 */
	public Project(String name, String description) {
		super();
		this.name = name;
		this.description = description;
		roleConfig = new RoleConfigMapImplementation();
	}

	/**
	 * @param name
	 * @param description
	 * @param projectRoleConfig
	 */
	public Project(String name, String description, ProjectRoleConfig projectRoleConfig) {
		this.name = name;
		this.description = description;
		roleConfig = projectRoleConfig;
	}
	
	/**
	 * @return
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return
	 */
	public ProjectRoleConfig getRoleConfig() {
		return roleConfig;
	}
	/**
	 * @param roleConfig
	 */
	public void setRoleConfig(ProjectRoleConfig roleConfig) {
		this.roleConfig = roleConfig;
	}
	
	

}
